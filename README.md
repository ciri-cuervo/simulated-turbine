# Simulated Turbine #
_An application to compare the performance of WebSocket and REST when displaying wind turbines data._

## What is this application about? ##

Information from different wind turbines is gathered from the a network via SNMP and stored into a SQL database. Users can look at this information in a web page. This project aims to display the information in real time, that's why two different approaches were developed STOMP over **WebSockets** and AJAX consuming a **REST Web Service**.

**WebSockets** allow a long-held single TCP socket connection to be established between the client and server which allows for bi-directional, full duplex, messages to be instantly distributed with little overhead resulting in a very low latency connection. In our application, client and server communicate via STOMP.

**STOMP** provides an interoperable wire format so that STOMP clients can communicate with any STOMP message broker to provide easy and widespread messaging interoperability among many languages, platforms and brokers.

With **AJAX**, web applications can retrieve data from a server asynchronously (in the background) without interfering with the display and behavior of the existing page. In our application, Ajax consumes a **REST web service** and the format of the messages is JSON.

## Implementation ##

Language: **Java 8**

Project configuration: **Maven**

Servlet container: **Tomcat**

Database: **MySQL**

SNMP: **SNMP4J**

All the back-end services are supported by **Spring Framework**.

* Object Relational Mapping (**Hibernate**)
* Data Access
* REST API
* WebSockets (**STOMP** over **SockJS**)
* Security
* Scheduled tasks
* HTML5 template engine (**Thymeleaf**)

UI design and services

* **jQuery**
* **Bootstrap**
* **Google Maps API**
* **Google Visualization API**

## Compile and run ##

As it is a maven project, to compile the application use the command:

```
#!bash

simulated-turbine$ mvn clean package
```

Once the jar file is generated in target directory run the application with the following command:

```
#!bash

target$ java -jar simulated-turbine-{version}.jar
```

Any property set in _application.properties_ file can be overridden by appending _--property=value_ as argument.

For example to change default port and context path:

```
#!bash

target$ java -jar simulated-turbine-{version}.jar --server.port=4567 --server.context-path=/st
```

This application will be available at: http://localhost:4567/st
