/**
 * 
 */
(function() {

	var websocketSrvc = {
		map : null,
		markers : {},
		infowindows : {},
		counter : 0,
		datatransf : 0,
		bandwidth : {
			lastMsgDate : -1,
			queue : []
		},

		initialize : function() {
			// create Google Map for websocket
			websocketSrvc.map = new google.maps.Map(document.getElementById('map-canvas-websocket'), JS.config.map);

			// create STOMP over SockJS client
			websocketSrvc.createWebsocketConn();
		},

		createWebsocketConn : function() {
			var socket = new SockJS(JS.config.contextPath + '/turbines-websocket');

			var client = Stomp.over(socket);
			client.debug = null;
			client.connect({}, function(frame) {
				// subscribe to turbine locations changes
				client.subscribe("/topic/turbine-locations", function(message) {
					$.each(JSON.parse(message.body), function(index, turbineLocation) {
						mapSrvc.updateMarker(websocketSrvc, turbineLocation);
					});
				});
				// subscribe to turbines changes
				client.subscribe("/topic/turbines", function(message) {
					$.each(JSON.parse(message.body), function(index, turbine) {
						mapSrvc.updateInfowindow(websocketSrvc.infowindows, turbine);
					});
				});
				// subscribe to single turbine changes
				client.subscribe("/topic/turbine.*", function(message) {
					var now = new Date().getTime();
					var turbine = JSON.parse(message.body);
					var responseHeaderSize = 120;
					var contentLength = parseInt(message.headers['content-length']);
					var totalSize = responseHeaderSize + contentLength;
					// update map
					mapSrvc.updateInfowindow(websocketSrvc.infowindows, turbine);
					// update messages counter
					$('#compare-counter-websocket').text(++websocketSrvc.counter);
					// update data transfered
					websocketSrvc.datatransf += totalSize;
					$('#compare-tranferred-websocket').text((websocketSrvc.datatransf / 1024).toFixed(2) + ' KB');
					// update bandwidth
					if (websocketSrvc.bandwidth.lastMsgDate != -1) {
						var secondsDifference = (now - websocketSrvc.bandwidth.lastMsgDate) / 1000;
						addToQueue(websocketSrvc.bandwidth.queue, [totalSize, secondsDifference]);
					}
					websocketSrvc.bandwidth.lastMsgDate = now;
					// update compare services section
					compareSrvc.updateComparisson(turbine, 'websocket');
					// update turbine data chart
					if (turbine.turbinename === JS.config.turbineToCompare
							&& $('.chart-date-picker input[name=chart-date]:checked').val() == 'current') {
						chartSrvc.updateChart(turbine);
					}
				});
				// subscribe to single turbine states
				client.subscribe("/state/turbine.*", function(message) {
					var turbineState = JSON.parse(message.body);
					mapSrvc.updateInfowindowState(websocketSrvc.infowindows, turbineState.turbinename, turbineState.state);
					mapSrvc.updateInfowindowState(webserviceSrvc.infowindows, turbineState.turbinename, turbineState.state);
				});
			}, function() {
				setTimeout(websocketSrvc.createWebsocketConn, 1000);
			});
		}
	};

	var webserviceSrvc = {
		map : null,
		markers : {},
		infowindows : {},
		counter : 0,
		datatransf : 0,
		bandwidth : {
			lastMsgDate : -1,
			queue : []
		},

		initialize : function() {
			// create Google Map for webservice
			webserviceSrvc.map = new google.maps.Map(document.getElementById('map-canvas-webservice'), JS.config.map);

			// get initial turbine locations information
			$.getJSON(JS.config.contextPath + '/turbine-locations', function(turbineLocations) {
				$.each(turbineLocations, function(index, turbineLocation) {
					mapSrvc.updateMarker(webserviceSrvc, turbineLocation);
				});
			});

			// get initial turbines information
			$.getJSON(JS.config.contextPath + '/turbines', function(turbines) {
				$.each(turbines, function(index, turbine) {
					mapSrvc.updateInfowindow(webserviceSrvc.infowindows, turbine);
				});
			});

			// get turbines information every: ajaxCallInterval
			setInterval(function() {
				$.getJSON(JS.config.contextPath + '/turbines', function(turbines) {
					var now = new Date().getTime();
					$.each(turbines, function(index, turbine) {
						// update map
						mapSrvc.updateInfowindow(webserviceSrvc.infowindows, turbine);
						// update compare services section
						compareSrvc.updateComparisson(turbine, 'webservice');
					});
					var requestHeaderSize = 350;
					var responseHeaderSize = 350;
					var contentLength = JSON.stringify(turbines).length;
					var totalSize = requestHeaderSize + responseHeaderSize + contentLength;
					// update data transfered
					webserviceSrvc.datatransf += totalSize;
					$('#compare-tranferred-webservice').text((webserviceSrvc.datatransf / 1024).toFixed(2) + ' KB');
					// update bandwidth
					if (webserviceSrvc.bandwidth.lastMsgDate != -1) {
						var secondsDifference = (now - webserviceSrvc.bandwidth.lastMsgDate) / 1000;
						addToQueue(webserviceSrvc.bandwidth.queue, [totalSize, secondsDifference]);
					}
					webserviceSrvc.bandwidth.lastMsgDate = now;
					// update DB queries
					$('#compare-db-webservice').text(webserviceSrvc.counter);
				});
				// update messages counter
				$('#compare-counter-webservice').text(++webserviceSrvc.counter);
			}, JS.config.ajaxCallInterval);
		}
	};

	var mapSrvc = {
		initializeOptions : function() {
			$('#toggleInfowindows').change(function() {
				if (this.checked) {
					$.each(websocketSrvc.infowindows, function(turbinename, infowindow) {
						infowindow.w.open(websocketSrvc.map, websocketSrvc.markers[turbinename]);
					});
					$.each(webserviceSrvc.infowindows, function(turbinename, infowindow) {
						infowindow.w.open(webserviceSrvc.map, webserviceSrvc.markers[turbinename]);
					});
				} else {
					$.each(websocketSrvc.infowindows, function(turbinename, infowindow) {
						infowindow.w.close();
					});
					$.each(webserviceSrvc.infowindows, function(turbinename, infowindow) {
						infowindow.w.close();
					});
				}
			});
		},

		updateMarker : function(service, turbineLocation) {
			var marker = service.markers[turbineLocation.turbinename];
			if (marker) {
				marker.setPosition({
					lat : turbineLocation.latitude,
					lng : turbineLocation.longitude
				});
			} else {
				marker = mapSrvc.addMarker(service, turbineLocation);
			}
		},

		addMarker : function(service, turbineLocation) {
			var marker = new google.maps.Marker({
				position : {
					lat : turbineLocation.latitude,
					lng : turbineLocation.longitude
				},
				icon : {
					path : google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
					scale : 8,
					fillColor : 'Beige',
					fillOpacity : 1.0,
					strokeColor : 'SaddleBrown',
					strokeOpacity : 0.5,
					strokeWeight : 4
				},
				map : service.map,
				animation : google.maps.Animation.DROP
			});

			var infowindow = new google.maps.InfoWindow({
				content : turbineLocation.turbinename
			});

			google.maps.event.addListener(marker, 'click', function() {
				if (JS.config.turbineToCompare === turbineLocation.turbinename) {
					return;
				}
				JS.config.turbineToCompare = turbineLocation.turbinename;
				$('#compare-turbinename').text(JS.config.turbineToCompare);
				compareSrvc.clearComparisson();
				chartSrvc.clearChart();
				chartSrvc.getAverageData();
			});
			google.maps.event.addListener(marker, 'mouseover', function() {
				infowindow.open(service.map, marker);
			});
			google.maps.event.addListener(marker, 'mouseout', function() {
				if ($('#toggleInfowindows').is(':checked')) {
					return;
				}
				infowindow.close();
			});

			service.markers[turbineLocation.turbinename] = marker;
			service.infowindows[turbineLocation.turbinename] = { w : infowindow, state : 'Link is up', stateClass : 'text-success' };

			return marker;
		},

		updateInfowindow : function(infowindows, turbine) {
			var infowindow = infowindows[turbine.turbinename];
			if (infowindow) {
				var content = "<div class=\"infowindow\"><h5>" + turbine.turbinename + "</h5>";
				content += "<ul><li><b>Update time: </b>" + new Date(turbine.date).toLocaleString() + "</li>";
				$.each(JS.config.turbineAttr, function(attribute, label) {
					var value = $.type(turbine[attribute]) === 'number' ? turbine[attribute].toFixed(2) : turbine[attribute];
					content += "<li><b>" + JS.config.turbineAttr[attribute] + ": </b>" + value + "</li>";
				});
				content += "</ul>";
				content += "<div id=\"state\" class=\"" + infowindow.stateClass + "\">" + infowindow.state + "</div>";
				content += "</div>";
				infowindow.w.setContent(content);
			}
		},

		updateInfowindowState : function(infowindows, turbinename, state) {
			var infowindow = infowindows[turbinename];
			if (infowindow) {
				var stateClass = (state === 'Link is up' ? 'text-success' : 'text-danger');

				var content = infowindow.w.getContent();
				var parsed = $('<div />').append(content);
				parsed.find('#state').html(state).removeClass().addClass(stateClass);

				infowindow.w.setContent(parsed.html());
				infowindow.state = state;
				infowindow.stateClass = stateClass;
			}
		}
	};

	var chartSrvc = {
		charts : {},
		datas : {},

		initialize : function() {
			// create Google charts
			$.each(JS.config.chartOptions, function(attribute, option) {
				var chart = new google.visualization.Gauge(document.getElementById('chart-' + attribute + '-canvas'));
				chartSrvc.charts[attribute] = chart;
				chartSrvc.datas[attribute] = google.visualization.arrayToDataTable([
					['Label', 'Value'],
					[JS.config.turbineAttr[attribute], 0]
				]);
			});
			// clear the charts
			chartSrvc.clearChart();
			// bind radio-inputs actions
			$('.chart-date-picker input[name=chart-date]').on('change', function() {
				if ($(this).is(':checked')) {
					chartSrvc.clearChart();
					chartSrvc.getAverageData();
				}
			});
		},

		updateChart : function(turbine) {
			if (turbine.turbinename != JS.config.turbineToCompare) {
				return;
			}
			// update Google chart
			$.each(JS.config.chartOptions, function(attribute, option) {
				chartSrvc.datas[attribute].setValue(0, 1, turbine[attribute].toFixed(2));
				chartSrvc.charts[attribute].draw(chartSrvc.datas[attribute], option);
			});
		},

		clearChart : function() {
			// update Google chart
			$.each(JS.config.chartOptions, function(attribute, option) {
				chartSrvc.datas[attribute].setValue(0, 1, 0.0);
				chartSrvc.charts[attribute].draw(chartSrvc.datas[attribute], option);
			});
		},
		
		getAverageData : function() {
			var selected = $('.chart-date-picker input[name=chart-date]:checked').val();
			if (selected == 'current') {
				return;
			}

			var dateFrom = new Date(), dateTo = new Date();
			switch (selected) {
				case 'yesterday':
					dateFrom.setDate(dateFrom.getDate() - 1);
					dateTo.setDate(dateTo.getDate() - 1);
					break;
				case 'last-week':
					var dayOfWeek = dateFrom.getDay(); // 0-6
					dateFrom.setDate(dateFrom.getDate() - dayOfWeek - 7);
					dateTo.setDate(dateFrom.getDate() + 6);
					break;
				case 'last-month':
					var dayOfMonth = dateFrom.getDate(); // 1-31
					dateFrom.setDate(1);
					dateFrom.setMonth(dateFrom.getMonth() - 1);
					dateTo.setDate(dateTo.getDate() - dayOfMonth);
					break;
				default: return;
			}

			// send Ajax request
			var url = JS.config.contextPath + '/turbines/' + JS.config.turbineToCompare + '/'
					+ dateFrom.toISOString().substring(0, 10) + '/' + dateTo.toISOString().substring(0, 10);
			console.log(url);
			$.getJSON(url, function(turbine) {
				// update turbine data chart
				chartSrvc.updateChart(turbine);
			});
		}
	};

	var compareSrvc = {
		initialize : function() {
			$('#compare-turbinename').text(JS.config.turbineToCompare);
			// update compare bandwidth
			setInterval(function() {
				if (websocketSrvc.bandwidth.queue.length > 0) {
					var avgBandwidth = getAvgBandwidth(websocketSrvc.bandwidth.queue.slice(0));
					$('#compare-bandwidth-websocket').text(avgBandwidth.toFixed(2) + ' B/s');
				}
				if (webserviceSrvc.bandwidth.queue.length > 0) {
					var avgBandwidth = getAvgBandwidth(webserviceSrvc.bandwidth.queue.slice(0));
					$('#compare-bandwidth-webservice').text(avgBandwidth.toFixed(2) + ' B/s');
				}
			}, 1000);
		},

		updateComparisson : function(turbine, service) {
			if (turbine.turbinename === JS.config.turbineToCompare) {
				var oldTurbineDate = $('#compare-turbinedate-' + service).text();
				var newTurbineDate = new Date(turbine.date).toLocaleString();
				$('#compare-turbinedate-' + service).text(newTurbineDate);
				if (newTurbineDate !== oldTurbineDate) {
					$('#compare-update-' + service).text(new Date().toLocaleString());
				}
			}
		},

		clearComparisson : function() {
			$('#compare-turbinedate-websocket').text('---');
			$('#compare-turbinedate-webservice').text('---');
			$('#compare-update-webservice').text('---');
			$('#compare-update-websocket').text('---');
		}
	};

	var addToQueue = function(queue, bandwidth) {
		if (queue.length == JS.config.bandwidth.maxQueueSize) {
			queue.pop();
		}
		queue.unshift(bandwidth);
	};

	var getAvgBandwidth = function(queue) {
		var size = 0;
		var seconds = 0;
		$.each(queue, function(index, bandwidth) {
			size += bandwidth[0];
			seconds += bandwidth[1];
		});
		return size / seconds;
	};

	$(function() {
		websocketSrvc.initialize();
		webserviceSrvc.initialize();
		mapSrvc.initializeOptions();
		chartSrvc.initialize();
		compareSrvc.initialize();
	});

})();
