TRUNCATE `turbine_location`;

INSERT INTO `turbine_location` (`turbinename`, `latitude`, `longitude`) VALUES
('Test Turbine 1', 52.167169, -8.484225),
('Test Turbine 2', 52.167350, -8.481033),
('Test Turbine 3', 52.166468, -8.482422),
('Test Turbine 4', 52.166581, -8.480276),
('Test Turbine 5', 52.166467, -8.485647),
('Cork Turbine171', 52.167199, -8.478799);
