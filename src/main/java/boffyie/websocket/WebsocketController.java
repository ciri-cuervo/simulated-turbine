package boffyie.websocket;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import boffyie.domain.Turbine;
import boffyie.domain.TurbineLocation;
import boffyie.service.TurbineLocationService;
import boffyie.service.TurbineService;

@Controller
public class WebsocketController {

	@Autowired
	private TurbineService turbineService;
	@Autowired
	private TurbineLocationService turbineLocationService;

	@SubscribeMapping("/turbines")
	@MessageMapping("/turbines")
	@SendTo("/topic/turbines")
	public List<Turbine> getTurbines()
	{
		return turbineService.findFirstEachByOrderByDateDesc();
	}

	@SubscribeMapping("/turbine-locations")
	@MessageMapping("/turbine-locations")
	@SendTo("/topic/turbine-locations")
	public List<TurbineLocation> getTurbineLocations()
	{
		return turbineLocationService.findAll();
	}

}
