package boffyie.util;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StreamUtils {

	/**
	 * Utility method to get distinct items from a stream based on a key.
	 */
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
	{
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	/**
	 * Turns an Optional<T> into a Stream<T> of length zero or one depending upon whether a value is present.
	 * 
	 * TODO Optional.stream has been added to JDK 9
	 */
	public static <T> Stream<T> streamopt(Optional<T> opt)
	{
		return opt.isPresent() ? Stream.of(opt.get()) : Stream.empty();
	}

}
