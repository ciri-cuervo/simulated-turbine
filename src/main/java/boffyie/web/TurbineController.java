package boffyie.web;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import boffyie.domain.Turbine;
import boffyie.domain.TurbineLocation;
import boffyie.service.TurbineLocationService;
import boffyie.service.TurbineService;

@RestController
public class TurbineController {

	// request mapping regular expressions
	private static final String ISO_DATE = "\\d{4}-[01]\\d-[0-3]\\d";
	private static final String UINT = "\\d+";

	@Autowired
	private TurbineService turbineService;
	@Autowired
	private TurbineLocationService turbineLocationService;

	@RequestMapping("/turbines")
	public List<Turbine> getAllTurbines()
	{
		return turbineService.findFirstEachByOrderByDateDesc();
	}

	@RequestMapping("/turbines/{turbinename}")
	public Turbine getTurbinesByTurbinename(@PathVariable String turbinename)
	{
		return turbineService.findFirstByTurbinenameOrderByDateDesc(turbinename)
				.orElseThrow(() -> new RuntimeException("turbine not found"));
	}

	@RequestMapping("/turbines/{turbinename}/{datefrom:" + ISO_DATE + "}/{dateto:" + ISO_DATE + "}")
	public Turbine getTurbineAverageData(@PathVariable String turbinename,
			@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate datefrom,
			@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate dateto)
	{
		return turbineService.getTurbineAverageData(turbinename, datefrom, dateto).orElse(null);
	}

	@RequestMapping("/turbines/{turbinename}/{date:" + ISO_DATE + "}/{daysbefore:" + UINT + "}")
	public Turbine getTurbineAverageData(@PathVariable String turbinename,
			@PathVariable @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@PathVariable int daysbefore)
	{
		return turbineService.getTurbineAverageData(turbinename, date, daysbefore).orElse(null);
	}

	@RequestMapping("/turbine-locations")
	public List<TurbineLocation> getAllTurbineLocations()
	{
		return turbineLocationService.findAll();
	}

	@RequestMapping("/turbine-locations/{turbinename}")
	public TurbineLocation getTurbineLocationByTurbinename(@PathVariable String turbinename)
	{
		return turbineLocationService.findByTurbinename(turbinename)
				.orElseThrow(() -> new RuntimeException("turbine not found"));
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(Exception.class)
	public void handleNotFound()
	{
		// Nothing to do
	}

}
