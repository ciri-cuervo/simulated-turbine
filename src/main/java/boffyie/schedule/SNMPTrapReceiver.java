package boffyie.schedule;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcher;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.TransportIpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.AbstractTransportMapping;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import boffyie.service.TurbineService;

@Component
public class SNMPTrapReceiver implements CommandResponder {

	private static final Logger LOG = LoggerFactory.getLogger(SNMPTrapReceiver.class);

	@Autowired
	private TurbineService turbineService;
	@Autowired
	private Environment environment;

	private AbstractTransportMapping transport;
	private Map<String, Boolean> turbineStates = new HashMap<>();

	@PostConstruct
	public void postConstruct() {
		try
		{
			LOG.info("Initializing trap receiver");
			TransportIpAddress address = new UdpAddress(environment.getProperty("trap.listen.address"));
			listen(address);
		} catch (IOException e)
		{
			LOG.error("Error initializing trap receiver", e);
		}
	}

	@PreDestroy
	public void preDestroy() {
		try
		{
			LOG.info("Closing trap receiver");
			if (transport != null)
			{
				transport.close();
			}
		} catch (IOException e)
		{
			LOG.error("Error closing trap receiver", e);
		}
	}

	/**
	 * Trap Listener
	 */
	public synchronized void listen(TransportIpAddress address) throws IOException
	{
		if (address instanceof TcpAddress)
		{
			transport = new DefaultTcpTransportMapping((TcpAddress) address);
		} else
		{
			transport = new DefaultUdpTransportMapping((UdpAddress) address);
		}

		ThreadPool threadPool = ThreadPool.create("DispatcherPool", 10);
		MessageDispatcher mDispathcher = new MultiThreadedMessageDispatcher(threadPool, new MessageDispatcherImpl());

		// add message processing models
		mDispathcher.addMessageProcessingModel(new MPv1());
		mDispathcher.addMessageProcessingModel(new MPv2c());

		// add all security protocols
		SecurityProtocols.getInstance().addDefaultProtocols();
		SecurityProtocols.getInstance().addPrivacyProtocol(new Priv3DES());

		// Create Target
		CommunityTarget target = new CommunityTarget();
		target.setCommunity(new OctetString("public"));

		Snmp snmp = new Snmp(mDispathcher, transport);
		snmp.addCommandResponder(this);

		transport.listen();
		LOG.info("Listening on {}", address);
	}

	/**
	 * This method will be called whenever a pdu is received on the given port specified in the
	 * listen() method
	 */
	public synchronized void processPdu(CommandResponderEvent cmdRespEvent)
	{
		LOG.warn("Wind Alert!!");
		PDU pdu = cmdRespEvent.getPDU();
		if (pdu != null)
		{
			String turbinename = "Cork Turbine171";
			if (!turbineStates.containsKey(turbinename) || turbineStates.get(turbinename).equals(Boolean.FALSE)) {
				turbineStates.put(turbinename, Boolean.TRUE);
				turbineService.notifyTurbineState(turbinename, "Link is down");
			} else {
				turbineStates.put(turbinename, Boolean.FALSE);
				turbineService.notifyTurbineState(turbinename, "Link is up");
			}
		}
	}

}
