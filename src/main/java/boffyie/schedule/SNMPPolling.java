package boffyie.schedule;

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import boffyie.domain.Turbine;
import boffyie.domain.TurbineRepository;
import boffyie.service.TurbineService;

@Component
public class SNMPPolling {

	private static final Logger LOG = LoggerFactory.getLogger(SNMPPolling.class);

	@Autowired
	private TurbineRepository turbineRepository;
	@Autowired
	private TurbineService turbineService;
	@Autowired
	private Environment environment;

	/*
	 * TODO DELETE THIS METHOD, IT IS JUST FOR TESTING
	 */
	@Scheduled(initialDelay = 1000, fixedRate = 5000)
	public void test()
	{
		for (int t = 1; t <= 5; t++)
		{
			Turbine turbine = new Turbine();
			turbine.setDate(System.currentTimeMillis());
			turbine.setTurbinename("Test Turbine " + t);
			turbine.setTurbinespeed(Math.random() * 100.0); // 0 to 100
			turbine.setWindspeed(Math.random() * 50.0); // 0 to 50
			turbine.setWinddirection(Math.random() * 360.0 - 180.0); // -180 to 180
			turbine.setCurrentpower(Math.random() * 100.0); // 0 to 100
			turbine.setEnergygenerated(Math.random() * 100.0); // 0 to 100
			turbine.setTipspeedratio(Math.random() * 100.0); // 0 to 100
			turbine.setModelsize("Model Test");
			turbine.setVendorname("Vendor Test");

			turbine = turbineRepository.saveAndFlush(turbine);

			// notify that a new turbine has been stored in DB
			turbineService.notifyTurbineUpdate(turbine);

			if (t < 5)
			{
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException e)
				{
					// do nothing
				}
			}
		}
	}

	@Scheduled(initialDelay = 1000, fixedRate = 10000)
	@SuppressWarnings("unchecked")
	public void gatherTurbineData()
	{
		try
		{
			Snmp snmp4j = new Snmp(new DefaultUdpTransportMapping());
			snmp4j.listen();
			Address add = new UdpAddress(environment.getProperty("snmp.target.address"));
			CommunityTarget target = new CommunityTarget();
			target.setAddress(add);
			target.setTimeout(500);
			target.setRetries(3);
			target.setCommunity(new OctetString("public"));
			target.setVersion(SnmpConstants.version2c);

			PDU request0 = new PDU();
			request0.setType(PDU.GETNEXT);
			PDU request1 = new PDU();
			request1.setType(PDU.GETNEXT);
			PDU request2 = new PDU();
			request2.setType(PDU.GETNEXT);
			PDU request3 = new PDU();
			request3.setType(PDU.GETNEXT);
			PDU request4 = new PDU();
			request4.setType(PDU.GETNEXT);
			PDU request5 = new PDU();
			request5.setType(PDU.GETNEXT);
			PDU request6 = new PDU();
			request6.setType(PDU.GETNEXT);
			PDU request7 = new PDU();
			request7.setType(PDU.GETNEXT);
			PDU request8 = new PDU();
			request8.setType(PDU.GETNEXT);

			OID oid0 = new OID(".1.3.6.1.2.1.2.2.1.1.0");
			request0.add(new VariableBinding(oid0));
			OID oid1 = new OID(".1.3.6.1.2.1.2.2.1.1.1");
			request1.add(new VariableBinding(oid1));
			OID oid2 = new OID(".1.3.6.1.2.1.2.2.1.1.2");
			request2.add(new VariableBinding(oid2));
			OID oid3 = new OID(".1.3.6.1.2.1.2.2.1.1.3");
			request3.add(new VariableBinding(oid3));
			OID oid4 = new OID(".1.3.6.1.2.1.2.2.1.1.4");
			request4.add(new VariableBinding(oid4));
			OID oid5 = new OID(".1.3.6.1.2.1.2.2.1.1.5");
			request5.add(new VariableBinding(oid5));
			OID oid6 = new OID(".1.3.6.1.2.1.2.2.1.1.6");
			request6.add(new VariableBinding(oid6));
			OID oid7 = new OID(".1.3.6.1.2.1.2.2.1.1.7");
			request7.add(new VariableBinding(oid7));
			OID oid8 = new OID(".1.3.6.1.2.1.2.2.1.1.8");
			request8.add(new VariableBinding(oid8));

			PDU responsePDU0 = null;
			PDU responsePDU1 = null;
			PDU responsePDU2 = null;
			PDU responsePDU3 = null;
			PDU responsePDU4 = null;
			PDU responsePDU5 = null;
			PDU responsePDU6 = null;
			PDU responsePDU7 = null;
			//PDU responsePDU8 = null;

			ResponseEvent responseEvent0;
			responseEvent0 = snmp4j.send(request0, target);
			ResponseEvent responseEvent1;
			responseEvent1 = snmp4j.send(request1, target);
			ResponseEvent responseEvent2;
			responseEvent2 = snmp4j.send(request2, target);
			ResponseEvent responseEvent3;
			responseEvent3 = snmp4j.send(request3, target);
			ResponseEvent responseEvent4;
			responseEvent4 = snmp4j.send(request4, target);
			ResponseEvent responseEvent5;
			responseEvent5 = snmp4j.send(request5, target);
			ResponseEvent responseEvent6;
			responseEvent6 = snmp4j.send(request6, target);
			ResponseEvent responseEvent7;
			responseEvent7 = snmp4j.send(request7, target);
		//	ResponseEvent responseEvent8;
			//responseEvent8 = snmp4j.send(request8, target);

			if (responseEvent0 != null)
			{
				responsePDU0 = responseEvent0.getResponse();
				responsePDU1 = responseEvent1.getResponse();
				responsePDU2 = responseEvent2.getResponse();
				responsePDU3 = responseEvent3.getResponse();
				responsePDU4 = responseEvent4.getResponse();
				responsePDU5 = responseEvent5.getResponse();
				responsePDU6 = responseEvent6.getResponse();
				responsePDU7 = responseEvent7.getResponse();
			//	responsePDU8 = responseEvent8.getResponse();

				if (responsePDU0 != null)
				{
					Vector<VariableBinding> tmpv0 = responsePDU0.getVariableBindings();
					Vector<VariableBinding> tmpv1 = responsePDU1.getVariableBindings();
					Vector<VariableBinding> tmpv2 = responsePDU2.getVariableBindings();
					Vector<VariableBinding> tmpv3 = responsePDU3.getVariableBindings();
					Vector<VariableBinding> tmpv4 = responsePDU4.getVariableBindings();
					Vector<VariableBinding> tmpv5 = responsePDU5.getVariableBindings();
					Vector<VariableBinding> tmpv6 = responsePDU6.getVariableBindings();
					Vector<VariableBinding> tmpv7 = responsePDU7.getVariableBindings();
			//		Vector<VariableBinding> tmpv8 = responsePDU8.getVariableBindings();

					if (tmpv0 != null && tmpv0.size() != 0)
					{
						for (int k = 0; k < tmpv0.size(); k++)
						{
							VariableBinding vb = tmpv0.get(k);
							VariableBinding vb1 = tmpv1.get(k);
							VariableBinding vb2 = tmpv2.get(k);
							VariableBinding vb3 = tmpv3.get(k);
							VariableBinding vb4 = tmpv4.get(k);
							VariableBinding vb5 = tmpv5.get(k);
							VariableBinding vb6 = tmpv6.get(k);
							VariableBinding vb7 = tmpv7.get(k);
						//	VariableBinding vb8 = tmpv8.get(k);

							if (vb.isException())
							{
								String errorstring = vb.getVariable().getSyntaxString();
								LOG.error("Error: {}", errorstring);
							} else
							{
								// String sOid = vb.getOid().toString();

								final Variable var = vb.getVariable();
								final Variable var1 = vb1.getVariable();
								final Variable var2 = vb2.getVariable();
								final Variable var3 = vb3.getVariable();
								final Variable var4 = vb4.getVariable();
								final Variable var5 = vb5.getVariable();
								final Variable var6 = vb6.getVariable();
								final Variable var7 = vb7.getVariable();
							//	final Variable var8 = vb8.getVariable();


								Turbine turbine = new Turbine();
								turbine.setDate(System.currentTimeMillis());
								turbine.setTurbinename("Cork Turbine171");
								turbine.setTurbinespeed(Double.valueOf(var.toString()));
								turbine.setWindspeed(Double.valueOf(var1.toString()));
								turbine.setWinddirection(Double.valueOf(var2.toString()));
								turbine.setCurrentpower(Double.valueOf(var3.toString()));
								turbine.setEnergygenerated(Double.valueOf(var4.toString()));
								turbine.setTipspeedratio(Double.valueOf(var5.toString()));
								turbine.setModelsize(var6.toString());
								turbine.setVendorname(var7.toString());

								LOG.info("Saving {} into database", turbine);
								turbine = turbineRepository.saveAndFlush(turbine);

								// notify that a new turbine has been stored in DB
								turbineService.notifyTurbineUpdate(turbine);
							}

						}

					} else
					{
						LOG.info("variable bindings is null or empty");
					}

				} else
				{
					LOG.info("response pdu is null");
				}

			} else
			{
				LOG.info("response event is null");
			}

		} catch (Exception e)
		{
			LOG.error("There was an error.", e);
		}
	}

}
