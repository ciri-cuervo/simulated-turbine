package boffyie.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TurbineLocationRepository extends JpaRepository<TurbineLocation, String> {

	Optional<TurbineLocation> findByTurbinename(String turbinename);

}
