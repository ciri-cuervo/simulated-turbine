package boffyie.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "wturbinedb")
@IdClass(TurbineId.class)
public class Turbine {

	@Id
	private long date;

	@Id
	private String turbinename;

	private double turbinespeed;

	private double windspeed;

	private double winddirection;

	private double currentpower;

	private double energygenerated;

	private double tipspeedratio;

	private String modelsize;

	private String vendorname;

	public Turbine()
	{
	}

	public Turbine(long date, String turbinename, double turbinespeed, double windspeed,
			double winddirection, double currentpower, double energygenerated,
			double tipspeedratio, String modelsize, String vendorname)
	{
		this.date = date;
		this.turbinename = turbinename;
		this.turbinespeed = turbinespeed;
		this.windspeed = windspeed;
		this.winddirection = winddirection;
		this.currentpower = currentpower;
		this.energygenerated = energygenerated;
		this.tipspeedratio = tipspeedratio;
		this.modelsize = modelsize;
		this.vendorname = vendorname;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public String getTurbinename()
	{
		return turbinename;
	}

	public void setTurbinename(String turbinename)
	{
		this.turbinename = turbinename;
	}

	public double getTurbinespeed()
	{
		return turbinespeed;
	}

	public void setTurbinespeed(double turbinespeed)
	{
		this.turbinespeed = turbinespeed;
	}

	public double getWindspeed()
	{
		return windspeed;
	}

	public void setWindspeed(double windspeed)
	{
		this.windspeed = windspeed;
	}

	public double getWinddirection()
	{
		return winddirection;
	}

	public void setWinddirection(double winddirection)
	{
		this.winddirection = winddirection;
	}

	public double getCurrentpower()
	{
		return currentpower;
	}

	public void setCurrentpower(double currentpower)
	{
		this.currentpower = currentpower;
	}

	public double getEnergygenerated()
	{
		return energygenerated;
	}

	public void setEnergygenerated(double energygenerated)
	{
		this.energygenerated = energygenerated;
	}

	public double getTipspeedratio()
	{
		return tipspeedratio;
	}

	public void setTipspeedratio(double tipspeedratio)
	{
		this.tipspeedratio = tipspeedratio;
	}

	public String getModelsize()
	{
		return modelsize;
	}

	public void setModelsize(String modelsize)
	{
		this.modelsize = modelsize;
	}

	public String getVendorname()
	{
		return vendorname;
	}

	public void setVendorname(String vendorname)
	{
		this.vendorname = vendorname;
	}

	@Override
	public String toString()
	{
		return "Turbine [date=" + date + ", turbinename=" + turbinename + ", turbinespeed="
				+ turbinespeed + ", windspeed=" + windspeed + ", winddirection=" + winddirection
				+ ", currentpower=" + currentpower + ", energygenerated=" + energygenerated
				+ ", tipspeedratio=" + tipspeedratio + ", modelsize=" + modelsize + ", vendorname="
				+ vendorname + "]";
	}

}
