package boffyie.domain;

import java.io.Serializable;

public class TurbineId implements Serializable {

	private static final long serialVersionUID = -3152552590628853228L;

	private long date;
	private String turbinename;

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public String getTurbinename()
	{
		return turbinename;
	}

	public void setTurbinename(String turbinename)
	{
		this.turbinename = turbinename;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (date ^ (date >>> 32));
		result = prime * result + ((turbinename == null) ? 0 : turbinename.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurbineId other = (TurbineId) obj;
		if (date != other.date)
			return false;
		if (turbinename == null)
		{
			if (other.turbinename != null)
				return false;
		} else if (!turbinename.equals(other.turbinename))
			return false;
		return true;
	}

}
