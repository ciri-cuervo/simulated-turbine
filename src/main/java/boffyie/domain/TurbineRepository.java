package boffyie.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface TurbineRepository extends JpaRepository<Turbine, TurbineId> {

	Optional<Turbine> findFirstByTurbinenameOrderByDateDesc(String turbinename);

	@Query("SELECT new boffyie.domain.Turbine(0L, t.turbinename, AVG(t.turbinespeed), AVG(t.windspeed),"
			+ " AVG(t.winddirection), AVG(t.currentpower), AVG(t.energygenerated), AVG(t.tipspeedratio), '', '')"
			+ " FROM Turbine t WHERE t.turbinename = ?1 AND t.date >= ?2 AND t.date < ?3"
			+ " GROUP BY t.turbinename")
	Optional<Turbine> getTurbineAverageData(String turbinename, long datefrom, long dateto);

}
