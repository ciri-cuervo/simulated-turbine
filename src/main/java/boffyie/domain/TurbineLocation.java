package boffyie.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TurbineLocation {

	@Id
	private String turbinename;

	private Double latitude;

	private Double longitude;

	public String getTurbinename()
	{
		return turbinename;
	}

	public void setTurbinename(String turbinename)
	{
		this.turbinename = turbinename;
	}

	public Double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	public Double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}

	@Override
	public String toString()
	{
		return "TurbineLocation [turbinename=" + turbinename + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}

}
