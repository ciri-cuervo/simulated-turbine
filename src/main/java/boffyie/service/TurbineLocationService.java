package boffyie.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import boffyie.domain.TurbineLocation;
import boffyie.domain.TurbineLocationRepository;

@Service
@Transactional(readOnly = true)
public class TurbineLocationService {

	@Autowired
	private TurbineLocationRepository turbineLocationRepository;

	/**
	 * 
	 * @return
	 */
	public List<TurbineLocation> findAll()
	{
		return turbineLocationRepository.findAll();
	}

	/**
	 * 
	 * @param turbinename
	 * @return
	 */
	public Optional<TurbineLocation> findByTurbinename(String turbinename)
	{
		return turbineLocationRepository.findByTurbinename(turbinename);
	}

}
