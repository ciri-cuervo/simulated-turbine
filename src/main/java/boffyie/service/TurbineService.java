package boffyie.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import boffyie.domain.Turbine;
import boffyie.domain.TurbineLocation;
import boffyie.domain.TurbineLocationRepository;
import boffyie.domain.TurbineRepository;
import boffyie.util.StreamUtils;

@Service
@Transactional(readOnly = true)
public class TurbineService {

	private static final long MILLIS_IN_A_DAY = 24 * 60 * 60 * 1000;

	@Autowired
	private TurbineRepository turbineRepository;
	@Autowired
	private TurbineLocationRepository turbineLocationRepository;
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	/**
	 * This service returns the most updated data for each turbine.
	 * 
	 * @return
	 */
	public List<Turbine> findFirstEachByOrderByDateDesc()
	{
		try (Stream<TurbineLocation> locations = turbineLocationRepository.findAll().stream())
		{
			return locations.flatMap(l -> StreamUtils.streamopt(
					turbineRepository.findFirstByTurbinenameOrderByDateDesc(l.getTurbinename())
					)).collect(Collectors.toList());
		}
	}

	/**
	 * This service returns the most updated data for a turbine.
	 * 
	 * @param turbinename
	 * @return
	 */
	public Optional<Turbine> findFirstByTurbinenameOrderByDateDesc(String turbinename)
	{
		return turbineRepository.findFirstByTurbinenameOrderByDateDesc(turbinename);
	}

	/**
	 * This service returns the average data for a turbine.<br>
	 * From {@code datefrom} at {@code 00:00} to {@code (dateto + 1)} at {@code 00:00}
	 * 
	 * @param turbinename
	 * @param datefrom
	 * @param dateto
	 * @return
	 */
	public Optional<Turbine> getTurbineAverageData(String turbinename, LocalDate datefrom, LocalDate dateto)
	{
		long datefromLong = datefrom.toEpochDay() * MILLIS_IN_A_DAY;
		long datetoLong = dateto.plusDays(1).toEpochDay() * MILLIS_IN_A_DAY;
		return turbineRepository.getTurbineAverageData(turbinename, datefromLong, datetoLong);
	}

	/**
	 * This service returns the average data for a turbine.<br>
	 * From {@code (date - daysbefore)} at {@code 00:00} to {@code date + 1} at {@code 00:00}
	 * 
	 * @param turbinename
	 * @param date
	 * @param daysbefore
	 * @return
	 */
	public Optional<Turbine> getTurbineAverageData(String turbinename, LocalDate date, long daysbefore)
	{
		return getTurbineAverageData(turbinename, date.minusDays(daysbefore), date);
	}

	/**
	 * This service sends turbine data to all subscribers.
	 * 
	 * @param turbine
	 */
	public void notifyTurbineUpdate(Turbine turbine)
	{
		String destination = "/topic/turbine." + turbine.getTurbinename();
		messagingTemplate.convertAndSend(destination, turbine);
	}

	/**
	 * This service sends a turbine state to all subscribers.
	 * 
	 * @param turbinename
	 * @param state
	 */
	public void notifyTurbineState(String turbinename, String state)
	{
		Map<String, String> message = new HashMap<String, String>();
		message.put("turbinename", turbinename);
		message.put("state", state);

		String destination = "/state/turbine." + turbinename;
		messagingTemplate.convertAndSend(destination, message);
	}

}
